package git

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"time"

	"github.com/apex/log"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/spf13/viper"
)

const gitDataDirectory = "./data/"
const defaultRemoteName = "origin"

func init() {
	viper.SetDefault("git.username", "")
	viper.SetDefault("git.password", "")
	viper.SetDefault("git.remote", "")
}

func getAuth() *http.BasicAuth {
	username := viper.GetString("git.username")
	password := viper.GetString("git.password")

	if username == "" || password == "" {
		return nil
	}

	return &http.BasicAuth{
		Username: username,
		Password: password,
	}
}

// Commit creates a commit in the current repository
func Commit(followerDifference, friendsDifference, tweetDifference int) {
	err := initializeGitRepository()
	if err != nil {
		log.Warn("The data folder could not be converted into a Git repository. Therefore, the versioning does not work as expected.")
		return
	}

	// Open after initialization
	repo, _ := git.PlainOpen(gitDataDirectory)
	w, _ := repo.Worktree()

	status, _ := w.Status()
	if status.IsClean() {
		return
	}

	log.Info("Committing new changes...")

	w.Add(".gitignore")
	w.AddGlob(viper.GetString("archive") + "*.csv")

	var (
		followerPrefix = "new"
		friendsPrefix  = "new"
	)

	if followerDifference < 0 {
		followerPrefix = "old"
		followerDifference = -followerDifference
	}

	if friendsDifference < 0 {
		friendsPrefix = "old"
		friendsDifference = -friendsDifference
	}

	commitMessage := fmt.Sprintf("%d new tweets, %d %s followers, %d %s friends",
		tweetDifference,
		followerDifference,
		followerPrefix,
		friendsDifference,
		friendsPrefix)

	commitTime := time.Now()
	w.Commit(commitMessage, &git.CommitOptions{
		Author: &object.Signature{
			Name:  "twarchive",
			Email: "twarchive@localhost",
			When:  commitTime,
		},
	})

	status, _ = w.Status()
	if !status.IsClean() {
		w.Add(viper.GetString("archive") + ".info.json")
		w.Commit("Profile information updated", &git.CommitOptions{
			Author: &object.Signature{
				Name:  "twarchive",
				Email: "twarchive@localhost",
				When:  commitTime,
			},
		})
	}

	if remote := viper.GetString("git.remote"); remote == "" {
		return
	}

	log.Info("Pushing changes to remote")
	err = repo.Push(&git.PushOptions{
		RemoteName: defaultRemoteName,
		Auth:       getAuth(),
	})

	if err != nil {
		log.WithError(err).Warn("Error during push")
	}
}

func initializeGitRepository() error {
	_, err := git.PlainOpen(gitDataDirectory)
	if err == nil {
		return nil
	}

	log.Info("The Git repository does not exist yet and will be created.")

	_, err = git.PlainInit(gitDataDirectory, false)

	if err != nil {
		return err
	}

	log.Debug("Creating default .gitignore")
	filename := filepath.Join(gitDataDirectory, ".gitignore")
	err = ioutil.WriteFile(filename, []byte("media/"), 0644)

	if err != nil {
		return err
	}

	return initializeRemote()
}

func initializeRemote() error {
	remote := viper.GetString("git.remote")
	if remote == "" {
		return nil
	}

	repo, err := git.PlainOpen(gitDataDirectory)
	if err != nil {
		return err
	}

	_, err = repo.Remote(defaultRemoteName)
	if err == nil {
		// Repo already exists, skipping
		return nil
	}

	w, err := repo.Worktree()
	if err != nil {
		return err
	}

	refspec := config.RefSpec("+refs/heads/*:refs/remotes/origin/*")

	log.Info("Creating new Git remote named " + defaultRemoteName)
	_, err = repo.CreateRemote(&config.RemoteConfig{
		Name:  defaultRemoteName,
		URLs:  []string{remote},
		Fetch: []config.RefSpec{refspec},
	})

	if err != nil {
		log.WithError(err).Warn("Error creating remote")
	}

	log.Info("Pulling from remote")
	w.Pull(&git.PullOptions{
		RemoteName: defaultRemoteName,
		Auth:       getAuth(),
	})

	return err
}
