package archive

import (
	"encoding/csv"
	"fmt"
	"time"
	"twarchive/internal/tweets"

	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/apex/log"
	"github.com/cavaliercoder/grab"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/spf13/viper"
)

type videoVariantSort []twitter.VideoVariant

func (a videoVariantSort) Len() int           { return len(a) }
func (a videoVariantSort) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a videoVariantSort) Less(i, j int) bool { return a[i].Bitrate > a[j].Bitrate }

const dataFolder = "data"

func getArchiveFile(shouldOverwrite bool, flag int) (*os.File, error) {
	return getFile(shouldOverwrite, flag, "csv")
}
func getJSONFile(shouldOverwrite bool, flag int) (*os.File, error) {
	return getFile(shouldOverwrite, flag, "info.json")
}
func getFile(shouldOverwrite bool, flag int, ending string) (*os.File, error) {
	os.Mkdir(dataFolder, 0755)

	filename := dataFolder + "/" + viper.GetString("archive") + "." + ending

	_, err := os.Stat(filename)

	if err == nil && shouldOverwrite {
		log.Infof("The file %s exists and will be overwritten", filename)
		os.Remove(filename)
	} else if os.IsNotExist(err) {
		log.Infof("The file %s will be created automatically.", filename)
	}

	file, err := os.OpenFile(filename, flag, 0644)
	if os.IsNotExist(err) {
		file.WriteString("id,created_at,source,text,media_count\n")
	}

	return file, err
}

// Append appends tweets to the CSV
func Append(tweets []twitter.Tweet) {
	file, err := getArchiveFile(false, os.O_APPEND|os.O_CREATE|os.O_WRONLY)

	if err != nil {
		log.Fatalf("Could not append to the archive file", err)
	}

	handle(tweets, file)
}

// Create creates the csv
func Create(tweets []twitter.Tweet, shouldOverwrite bool) {
	file, err := getArchiveFile(shouldOverwrite, os.O_CREATE|os.O_RDWR)

	if err != nil {
		log.Fatalf("Could not create the archive file", err)
	}

	handle(tweets, file)
}

func handle(tweets []twitter.Tweet, file *os.File) {
	includeRetweets := viper.GetBool("include_retweets")
	log.Infof("Writing tweets to %s", file.Name())

	sort.Sort(wrapperSortDesc(tweets))
	writer := csv.NewWriter(file)

	for _, tweet := range tweets {
		if !includeRetweets && len(tweet.FullText) > 4 && tweet.FullText[:4] == "RT @" {
			continue
		}

		content := tweet.FullText

		startIndex := strings.Index(tweet.Source, ">") + 1
		endIndex := strings.LastIndex(tweet.Source, "<")
		source := tweet.Source[startIndex:endIndex]

		// Replace URLs
		for _, url := range tweet.Entities.Urls {
			content = strings.Replace(content, url.URL, url.ExpandedURL, 1)
		}

		mediaCount := 0
		mediaSuffix := "i" // i for images, v for videos, g for gifs

		for _, media := range tweet.Entities.Media {
			ending := "jpg"
			url := ""

			switch media.Type {
			case "photo":
				mediaSuffix = "i"
			case "animated_gif":
				mediaSuffix = "g"
			default:
				mediaSuffix = "v"
			}

			if media.Type == "photo" {
				url = media.MediaURLHttps
			} else {
				ending = "mp4"

				// Sort the variants descending by bitrate
				sort.Sort(videoVariantSort(media.VideoInfo.Variants))
				url = media.VideoInfo.Variants[0].URL
			}

			if url != "" && !viper.GetBool("skip_downloads") {
				// Remove URL from tweet text
				strings.Replace(content, url, "", 1)

				// Perform download request
				destination := fmt.Sprintf(dataFolder+"/media/%s_%d.%s", tweet.IDStr, mediaCount, ending)
				log.Infof("Downloading to %s from %s", destination, url)
				_, _ = grab.Get(destination, url)

				mediaCount++
			}
		}

		values := []string{tweet.IDStr, tweet.CreatedAt, source, strings.TrimSpace(content), strconv.Itoa(mediaCount) + mediaSuffix}
		writer.Write(values)
	}

	writer.Flush()
	file.Close()
}

// GetLastTweetID returns the newest entry in the CSV file
func GetLastTweetID() (int64, error) {
	file, err := getArchiveFile(false, os.O_RDONLY)

	if err != nil {
		return -1, err
	}

	reader := csv.NewReader(file)

	// Iterate over all lines
	var lastCreated time.Time = time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC)
	var lastID string = ""
	for {
		record, _ := reader.Read()

		// End of list reached
		if record == nil {
			break
		}

		t := tweets.Tweet{
			ID:         record[0],
			CreatedAt:  record[1],
			Source:     record[2],
			Text:       record[3],
			MediaCount: record[4],
		}
		if t.Created().After(lastCreated) {
			lastCreated = t.Created()
			lastID = t.ID
		}
	}

	return strconv.ParseInt(lastID, 10, 64)
}

type wrapperSortDesc []twitter.Tweet

func (a wrapperSortDesc) Len() int      { return len(a) }
func (a wrapperSortDesc) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a wrapperSortDesc) Less(i, j int) bool {
	first, _ := a[i].CreatedAtTime()
	second, _ := a[j].CreatedAtTime()

	return first.Before(second)
}
