package archive

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"io"
	"os"

	"github.com/dghubble/go-twitter/twitter"
)

// A TwitterProfileInformation contains all relevant data for a profile
type TwitterProfileInformation struct {
	Handle      string
	DisplayName string
	Biography   string
	Location    string
	URL         string
}

// SaveProfileInformation saves the current profile information
func SaveProfileInformation(info TwitterProfileInformation) {
	file, _ := getJSONFile(true, os.O_CREATE|os.O_RDWR)
	jsonBytes, _ := json.MarshalIndent(info, "", "  ")

	file.Write(jsonBytes)
	file.Close()
}

// SaveFollowers saves the followers as CSV and returns the difference of new lines
func SaveFollowers(followers *[]twitter.User) int {
	return saveUsers(followers, "followers")
}

// SaveFollowings saves the followings as CSV and returns the difference of new lines
func SaveFollowings(followings *[]twitter.User) int {
	return saveUsers(followings, "followings")
}

func saveUsers(users *[]twitter.User, filename string) int {
	file, _ := getFile(true, os.O_CREATE|os.O_RDWR, filename+".csv")
	defer file.Close()

	var (
		previousLines = 0
		afterLines    = 0
	)
	previousLines, _ = lineCounter(file)

	w := csv.NewWriter(file)
	defer w.Flush()

	w.Write([]string{"Handle", "Name"})
	for _, user := range *users {
		if user.ScreenName == "" {
			continue
		}

		values := []string{user.ScreenName, user.Name}
		w.Write(values)
	}

	afterLines, _ = lineCounter(file)
	return afterLines - previousLines
}

// source: https://stackoverflow.com/a/24563853/6565273
func lineCounter(r io.Reader) (int, error) {
	buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}

	for {
		c, err := r.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		switch {
		case err == io.EOF:
			return count, nil

		case err != nil:
			return count, err
		}
	}
}
