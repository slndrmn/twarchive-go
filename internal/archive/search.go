package archive

import (
	"encoding/csv"

	"os"
	"sort"
	"strings"
	"twarchive/internal/tweets"

	"github.com/apex/log"
)

// Function for error """handling""".
func checkForError(err error) {
	if err != nil {
		log.WithError(err).Fatal("Unknown error happened")
	}
}

// Search for tweets matching the given criteria.
func Search(query string, queryType string, caseSensitive bool, descending bool) []tweets.Tweet {
	// IO stuff
	file, err := getArchiveFile(false, os.O_RDONLY)
	checkForError(err)
	reader := csv.NewReader(file)

	// Create empty list of tweets
	list := make([]tweets.Tweet, 0)

	// Fetch through list of all tweets
	for {
		record, err := reader.Read()

		// End of list reached
		if record == nil {
			break
		}
		checkForError(err)

		// Create a new tweet
		t := tweets.Tweet{
			ID:         record[0],
			CreatedAt:  record[1],
			Source:     record[2],
			Text:       record[3],
			MediaCount: record[4],
		}

		appendTweet := false

		switch queryType {
		case "content":
			if caseSensitive {
				if strings.Contains(t.Text, query) {
					appendTweet = true
				}
			} else {
				if strings.Contains(strings.ToLower(t.Text), strings.ToLower(query)) {
					appendTweet = true
				}
			}

		case "client":
			if t.Source == query {
				appendTweet = true
			}
		}

		if appendTweet {
			list = append(list, t)
		}
	}

	file.Close()
	if !descending {
		sort.Sort(tweets.ByDateAsc(list))
	} else {
		sort.Sort(tweets.ByDateDesc(list))
	}

	return list
}
