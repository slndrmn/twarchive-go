package tweets

import "time"

// Tweet holds information about a single tweet.
type Tweet struct {
	ID         string `json:"id_str"`
	Source     string `json:"source"`
	CreatedAt  string `json:"created_at"`
	Text       string `json:"full_text"`
	MediaCount string
}

// ByDateAsc implements sort.Interface for sorting date strings ascendingly.
type ByDateAsc []Tweet

func (t ByDateAsc) Len() int           { return len(t) }
func (t ByDateAsc) Less(i, j int) bool { return t[i].Created().Before(t[j].Created()) }
func (t ByDateAsc) Swap(i, j int)      { t[i], t[j] = t[j], t[i] }

// ByDateDesc implements sort.Interface for sorting date strings descendingly.
type ByDateDesc []Tweet

func (t ByDateDesc) Len() int           { return len(t) }
func (t ByDateDesc) Less(i, j int) bool { return t[i].Created().After(t[j].Created()) }
func (t ByDateDesc) Swap(i, j int)      { t[i], t[j] = t[j], t[i] }

// ResultPageData holds the information that need to be parsed to the results page.
type ResultPageData struct {
	Count     int
	Query     string
	QueryType string
	Tweets    []Tweet
}

// Created returns the parsed creation date
func (t Tweet) Created() time.Time {
	// Example value: Wed Sep 30 18:47:25 +0000 2015
	result, _ := time.Parse("Mon Jan 2 15:04:05 -0700 2006", t.CreatedAt)
	return result
}

// CreatedReadable returns a human readable date format
func (t Tweet) CreatedReadable() string {
	return t.Created().Format("2006-01-02 15:04:05")
}

// URL of the tweet
func (t Tweet) URL() string {
	return "https://twitter.com/tweet/status/" + t.ID
}
