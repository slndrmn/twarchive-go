module twarchive

go 1.14

require (
	github.com/apex/log v1.4.0
	github.com/cavaliercoder/grab v2.0.0+incompatible
	github.com/dghubble/go-twitter v0.0.0-20190719072343-39e5462e111f
	github.com/dghubble/oauth1 v0.6.0
	github.com/fatih/color v1.9.0
	github.com/go-git/go-git/v5 v5.1.0
	github.com/gookit/color v1.2.5
	github.com/gorilla/mux v1.7.4
	github.com/mattn/go-colorable v0.1.7
	github.com/robfig/cron/v3 v3.0.1
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.0
)
