FROM golang:1.14-alpine AS build
WORKDIR /app
COPY . .
RUN go build

FROM alpine
WORKDIR /app
RUN mkdir data
COPY --from=build /app/twarchive .
COPY static/ /app/static/
COPY templates/ /app/templates/

VOLUME ["/app/data", "/app/config.yaml"]
ENTRYPOINT ["/app/twarchive"]
CMD ["web", "--hostname=0.0.0.0"]
