package main

import (
	"twarchive/cmd"

	console "twarchive/internal/log"

	"github.com/apex/log"
)

func main() {
	log.SetHandler(console.Default)
	cmd.Execute()
}
