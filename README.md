# twarchive - A Go reimplementation of TwArchive

## Build

```bash
go build
```

This will create a single executable twarchive file.

## Setup
twarchive provides several commands for the setup. If you've used the previous version (made in Python), you can simply copy & paste the 'data/' directory to the main folder of twarchive. The available commands can always be fetched with `twarchive help`.

## Usage
Simply invoke the `twarchive` command and it will show you all possible parameters and commands you can execute. For example, if you want to run the webserver, you can use the following command:

```
$ twarchive web
```

## Configuration
By default, twarchive is able to read existing archive files. In order to use the `fetch` command, which updates the archive file with the newest tweets from your Twitter account, you need to provide the API keys. By invoking `twarchive generate`, the default config is written to the `config.yaml`. Within this file, you can now provide the necessary API tokens.