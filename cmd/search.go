package cmd

import (
	"twarchive/internal/archive"

	"github.com/gookit/color"
	"github.com/spf13/cobra"
)

var (
	queryType     string
	caseSensitive bool
	reverseSearch bool
)

// searchCmd represents the search command
var searchCmd = &cobra.Command{
	Use:   "search <query>",
	Short: "Provides a CLI interface for the search",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		query := args[0]
		results := archive.Search(query, queryType, caseSensitive, reverseSearch)

		for _, tweet := range results {
			println()
			color.LightCyan.Print(tweet.CreatedReadable())
			color.Print(" | ")
			color.LightBlue.Println(tweet.URL())
			color.White.Println("  " + tweet.Text)
		}
	},
}

func init() {
	searchCmd.Flags().StringVarP(&queryType, "type", "t", "content", "The query type. Can be 'content' or 'client'.")
	searchCmd.Flags().BoolVarP(&caseSensitive, "case-sensitive", "c", false, "Whether or not the search should be case sensitive")
	searchCmd.Flags().BoolVarP(&reverseSearch, "reverse", "r", false, "If set to true, the newest tweets will be shown first.")

	rootCmd.AddCommand(searchCmd)
}
