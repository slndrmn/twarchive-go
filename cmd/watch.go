package cmd

import (
	"github.com/apex/log"
	"github.com/robfig/cron/v3"
	"github.com/spf13/cobra"
)

var expression string
var watchCmd = &cobra.Command{
	Use:   "watch",
	Short: "Executes the fetch automatically with the given expression and stays in the foreground",
	Run: func(cmd *cobra.Command, args []string) {
		c := cron.New()
		c.AddFunc(expression, func() {
			log.Info("Checking for new tweets...")

			Fetch()
		})

		log.WithField("expression", expression).Info("Monitoring for changes")
		c.Run()
	},
}

func init() {
	rootCmd.AddCommand(watchCmd)
	watchCmd.Flags().StringVarP(&expression, "expression", "e", "@hourly", "The cron expression that should be used for monitoring")
	watchCmd.Flags().StringVarP(&username, "username", "u", "", "The handle of the twitter user")
}
