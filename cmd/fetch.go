package cmd

import (
	"time"
	"twarchive/internal/archive"
	"twarchive/internal/git"

	"github.com/apex/log"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type twitterAPISettings struct {
	ConsumerKey    string
	ConsumerSecret string
	AccessToken    string
	AccessSecret   string
}

const twitterAPIMaxLimit = 200

func (s twitterAPISettings) Validate() {
	const expectedMinLength = 20

	if len(s.ConsumerKey) < expectedMinLength {
		log.Fatal("The consumer key is not valid")
	} else if len(s.ConsumerSecret) < expectedMinLength {
		log.Fatal("The consumer secret is not valid")
	} else if len(s.AccessToken) < expectedMinLength {
		log.Fatal("The access token is not valid")
	} else if len(s.AccessSecret) < expectedMinLength {
		log.Fatal("The access secret is not valid")
	}
}

var username string

// fetchCmd represents the fetch command
var fetchCmd = &cobra.Command{
	Use:   "fetch",
	Short: "Fetches the newest tweets for the account",
	Run: func(cmd *cobra.Command, args []string) {
		Fetch()
	},
}

// Fetch fetches the newest tweets
func Fetch() {
	var apiKeys twitterAPISettings
	apiKeys.AccessToken = viper.GetString("api.access_token")
	apiKeys.AccessSecret = viper.GetString("api.access_secret")
	apiKeys.ConsumerKey = viper.GetString("api.consumer_key")
	apiKeys.ConsumerSecret = viper.GetString("api.consumer_secret")

	apiKeys.Validate()

	config := oauth1.NewConfig(apiKeys.ConsumerKey, apiKeys.ConsumerSecret)
	token := oauth1.NewToken(apiKeys.AccessToken, apiKeys.AccessSecret)
	httpClient := config.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)

	var (
		trimUser = false
		params   = &twitter.UserTimelineParams{
			IncludeRetweets: &includeRetweets,
			ScreenName:      username,
			TrimUser:        &trimUser,
			TweetMode:       "extended",
			Count:           twitterAPIMaxLimit,
		}
	)

	lastTweet, _ := archive.GetLastTweetID()

	if lastTweet > 0 {
		log.Infof("Last tweet ID: %v", lastTweet)
		params.SinceID = lastTweet
	}

	var (
		totalTweetCount int   = 0
		maxID           int64 = 0
	)

	// The loop goes backwards until lastTweet,
	// since Twitter is always returning the newest 200 tweets.
	// And because the twitter API seems to be trash, this could indeed result in duplicates,
	// although they should not happen often.
	for {
		maxIDbefore := maxID
		tweets, _, err := client.Timelines.UserTimeline(params)
		if err != nil {
			log.WithError(err).Error("Could not fetch the user timeline")
			break
		}

		count := len(tweets)
		if count == 0 {
			log.Infof("No more tweets found.")
			break
		}
		totalTweetCount += count

		log.Infof("Fetched %d new tweets", count)
		var oldestTweetTime time.Time
		oldestTweetTime, _ = tweets[0].CreatedAtTime()
		for _, tweet := range tweets {
			tweetTime, _ := tweet.CreatedAtTime()
			if tweetTime.Before(oldestTweetTime) {
				oldestTweetTime = tweetTime
				maxID = tweet.ID
			}
		}

		// If repeating the loop two times with the same parameter, break.
		if maxID == maxIDbefore {
			break
		}

		params.MaxID = maxID
		archive.Append(tweets)
	}

	if username == "" {
		log.Warn("You need to provide a username if you want to save the personal information (username, name, biography).")
	}
	user, _, _ := client.Users.Show(&twitter.UserShowParams{
		ScreenName: username,
	})

	archive.SaveProfileInformation(archive.TwitterProfileInformation{
		DisplayName: user.Name,
		Handle:      user.ScreenName,
		Biography:   user.Description,
		Location:    user.Location,
		URL:         user.URL,
	})

	followers := getFollowers(client)
	friends := getFollowings(client)

	followerDifference := archive.SaveFollowers(followers)
	friendsDifference := archive.SaveFollowings(friends)

	git.Commit(followerDifference, friendsDifference, totalTweetCount)
}

func getFollowers(client *twitter.Client) *[]twitter.User {
	result := make([]twitter.User, 10)
	data, _, err := client.Followers.List(&twitter.FollowerListParams{
		ScreenName: username,
		Count:      twitterAPIMaxLimit,
	})

	if err != nil {
		log.WithError(err).Warn("Could not fetch the followings/friends")
	}

	result = append(result, data.Users...)
	for data.NextCursor != 0 {
		data, _, err = client.Followers.List(&twitter.FollowerListParams{
			ScreenName: username,
			Count:      twitterAPIMaxLimit,
			Cursor:     data.NextCursor,
		})
		result = append(result, data.Users...)
	}
	return &result
}

func getFollowings(client *twitter.Client) *[]twitter.User {
	result := make([]twitter.User, 10)
	data, _, err := client.Friends.List(&twitter.FriendListParams{
		ScreenName: username,
		Count:      twitterAPIMaxLimit,
	})

	if err != nil {
		log.WithError(err).Warn("Could not fetch the followings/friends")
	}

	result = append(result, data.Users...)
	for data.NextCursor != 0 {
		data, _, err = client.Friends.List(&twitter.FriendListParams{
			ScreenName: username,
			Count:      twitterAPIMaxLimit,
			Cursor:     data.NextCursor,
		})
		result = append(result, data.Users...)
	}

	return &result
}

func init() {
	fetchCmd.Flags().StringP("archive", "", "tweets", "The name of the configuration file. The '.csv' suffix is added automatically.")
	fetchCmd.MarkFlagFilename("archive")
	fetchCmd.Flags().StringVarP(&username, "username", "u", "", "The handle of the twitter user")
	fetchCmd.Flags().IntP("count", "c", 200, "The number of tweets to fetch")
	fetchCmd.Flags().BoolP("include-retweets", "", false, "Whether or not retweets should be included")

	viper.BindPFlag("archive", fetchCmd.Flags().Lookup("archive"))
	viper.BindPFlag("include_retweets", fetchCmd.Flags().Lookup("include-retweets"))

	viper.SetDefault("api.access_token", "")
	viper.SetDefault("api.access_secret", "")
	viper.SetDefault("api.consumer_key", "")
	viper.SetDefault("api.consumer_secret", "")

	rootCmd.AddCommand(fetchCmd)
}
