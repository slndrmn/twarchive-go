package cmd

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
	"twarchive/internal/archive"

	"github.com/apex/log"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	shouldOverwrite bool
	includeRetweets bool
)

// setupCmd represents the setup command
var setupCmd = &cobra.Command{
	Use:   "setup [path of tweet.js]",
	Short: "Initializes the .csv archive by providing a Twitter archive file",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		f, err := os.Open(args[0])
		if err != nil {
			log.WithError(err).
				Fatal("Could not open the tweet.js")
		}

		prepareFile(args[0])

		data, _ := ioutil.ReadAll(f)

		var obj []TwitterTweetWrapper

		// Read JSON into obj
		err = json.Unmarshal(data, &obj)
		if err != nil {
			log.WithError(err).
				Fatal("Could not unmarshal the JSON")
		}

		log.Infof("Number of tweets: %v", len(obj))
		var tweets = make([]twitter.Tweet, len(obj))
		for k, wrapper := range obj {
			tweets[k] = wrapper.Tweet
		}

		// Process the tweets
		archive.Create(tweets, shouldOverwrite)

		f.Close()
	},
}

func init() {
	setupCmd.Flags().StringP("archive", "", "tweets", "The name of the configuration file. The '.csv' suffix is added automatically.")
	setupCmd.MarkFlagFilename("archive")
	setupCmd.Flags().BoolVarP(&shouldOverwrite, "overwrite", "", false, "Whether or not existing files should be overwritten")
	setupCmd.Flags().BoolP("include-retweets", "", false, "Whether or not retweets should be included")
	setupCmd.Flags().BoolP("skip-downloads", "", false, "Whether or not media download should be skipped")

	viper.BindPFlag("archive", setupCmd.Flags().Lookup("archive"))
	viper.BindPFlag("include_retweets", setupCmd.Flags().Lookup("include-retweets"))
	viper.BindPFlag("skip_downloads", setupCmd.Flags().Lookup("skip-downloads"))

	rootCmd.AddCommand(setupCmd)
}

func prepareFile(filepath string) {
	input, err := ioutil.ReadFile(filepath)
	if err != nil {
		log.WithError(err).
			Fatal("Could not prepare the file")
	}

	// Remove the JS prefix
	output := strings.Replace(string(input), "window.YTD.tweet.part0 = ", "", 1)

	// Parse the JSON and cast the string properties to proper numbers.
	// The Twitter API returns these as integers, while the archive file contains these
	// as strings. (seriously, Twitter, I hate you)

	// indices
	r := regexp.MustCompile(`(?m)"(display_text_range|indices|aspect_ratio)"\s+:\s+\[\s+\"(\d+)\",\s+\"(\d+)\"\s+\]`)
	output = r.ReplaceAllString(output, "\"$1\":[$2,$3]")

	// IDs
	r = regexp.MustCompile(`(?m)"(id|favorite_count|retweet_count|source_status_id|w|h|in_reply_to_user_id|in_reply_to_status_id|bitrate|duration_millis)"\s+:\s+"(-?\d+)"`)
	output = r.ReplaceAllString(output, "\"$1\": $2")

	err = ioutil.WriteFile(filepath, []byte(output), 0644)
	if err != nil {
		log.WithError(err).
			Error("Could not write the archive file")
	}
}

// TwitterTweetWrapper ignore
type TwitterTweetWrapper struct {
	Tweet twitter.Tweet `json:"tweet"`
}
