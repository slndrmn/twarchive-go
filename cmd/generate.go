package cmd

import (
	"os"

	"github.com/apex/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// DefaultFilename returns the default filename for config files
const DefaultFilename string = "config.yaml"

var update bool
var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Generates the default config file",
	Long:  "Generates the config file (config.yaml) that allows further customization.",
	Run: func(cmd *cobra.Command, args []string) {

		/* Touch file if it does not exist.

			As stated in https://github.com/spf13/viper/pull/763 the documentation of viper
		    is wrong. The file needs to be created manually.
		*/
		_, err := os.Stat(DefaultFilename)
		if os.IsNotExist(err) {
			os.Create(DefaultFilename)
			update = true
		}

		if update {
			err = viper.WriteConfig()
		} else {
			err = viper.SafeWriteConfig()
		}

		if err != nil {
			log.Fatal("The configuration file does already exists. Use the --update option if you want to update it.")
		}
	},
}

func init() {
	generateCmd.Flags().BoolVarP(&update, "update", "", false, "Whether the current configuration file should be updated with new configuration values")
	rootCmd.AddCommand(generateCmd)
}
