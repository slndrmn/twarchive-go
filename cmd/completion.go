package cmd

import (
	"os"

	"github.com/apex/log"
	"github.com/spf13/cobra"
)

var completionCmd = &cobra.Command{
	Use:   "completion <bash|zsh|ps>",
	Short: "Generates bash completion scripts",
	Long: `To load completion run

. <(twarchive completion <bash|zsh|ps>)

To configure your bash shell to load completions for each session add to your bashrc

# ~/.bashrc or ~/.profile
. <(twarchive completion <bash|zsh|ps>)
`,
	Args: cobra.ExactArgs(1),

	Run: func(cmd *cobra.Command, args []string) {
		switch args[0] {
		case "bash":
			rootCmd.GenBashCompletion(os.Stdout)
		case "zsh":
			rootCmd.GenZshCompletion(os.Stdout)
		case "ps":
			rootCmd.GenPowerShellCompletion(os.Stdout)
		default:
			log.Error("This is not a valid shell type: " + args[0])
		}
	},
}

func init() {
	rootCmd.AddCommand(completionCmd)
}
