package cmd

import (
	"net/http"
	"strconv"
	"text/template"
	"twarchive/internal/archive"
	"twarchive/internal/tweets"

	"github.com/spf13/viper"

	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
)

var (
	port     int    = 5001
	hostname string = "localhost"
)

// webCmd represents the serve command
var webCmd = &cobra.Command{
	Use:   "web",
	Short: "Starts the web interface",
	Long: `Starts the web interface on the given port.

This allows an easy access to the tweet archive.`,
	Run: func(cmd *cobra.Command, args []string) {
		// Read configuration
		hostname = viper.GetString("web.hostname")
		port = viper.GetInt("web.port")

		// Routing
		r := mux.NewRouter()
		r.HandleFunc("/", index).Methods("GET", "POST")

		// Providing static files
		s := http.StripPrefix("/static/", http.FileServer(http.Dir("./static/")))
		r.PathPrefix("/static/").Handler(s)
		r.Handle("/", r)

		// Start server
		finalHost := hostname + ":" + strconv.Itoa(port)

		println("The webserver is now listening on: http://" + finalHost)
		http.ListenAndServe(finalHost, r)
	},
}

func init() {
	webCmd.Flags().StringVarP(&hostname, "hostname", "", "localhost", "The hostname of the server")
	webCmd.Flags().IntVarP(&port, "port", "p", 5001, "The port for the http server")

	// Set config values in "web" subsection
	viper.BindPFlag("web.hostname", webCmd.Flags().Lookup("hostname"))
	viper.BindPFlag("web.port", webCmd.Flags().Lookup("port"))

	rootCmd.AddCommand(webCmd)
}

func index(w http.ResponseWriter, r *http.Request) {
	templ := template.Must(template.ParseFiles("templates/index.html"))

	if r.Method == "POST" {
		query := r.FormValue("query")
		caseSensitive := r.FormValue("caseSensitive") == "on"
		reverseSearch := r.FormValue("reverseSearch") == "on"
		queryType := "content"

		// Search for tweets
		results := archive.Search(query, queryType, caseSensitive, reverseSearch)

		// Prepare data to be parsed to the HTML template
		data := tweets.ResultPageData{
			Query:     query,
			QueryType: queryType,
			Tweets:    results,
			Count:     len(results),
		}

		templ.Execute(w, data)
	} else {
		templ.Execute(w, nil)
	}
}
